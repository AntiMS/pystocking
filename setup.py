#!/usr/bin/env python3

from distutils.core import setup


setup(
    name="PyStocking",
    version="0.1",
    description="A simple SOCKS4a library for Python version 3.",
    author="AntiMS",
    author_email="antims923@gmail.com",
    url="http://github.com/AntiMS/pystocking",
    py_modules=['pystocking'],
)
