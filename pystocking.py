import struct
import socket


class Socks4aSocket(socket.socket):
    """
    A socket-like object which makes connections through a given SOCKS4a proxy.
    """
    def __init__(self, host, port):
        """
        Parameters
        ==========
        host
            The SOCKS4a server.

        port
            The SOCKS4a server port.
        """
        super(Socks4aSocket, self).__init__()
        self.host = host
        self.port = port

    def connect(self, address):
        super(Socks4aSocket, self).connect((self.host, self.port))
        self._negotiate(address)

    def _negotiate(self, address):
        host, port = address
        # Version and command.
        req = b"\x04\x01"
        # Port
        req += struct.pack(">H", port)
        # IP field
        try:
            resolve = False
            req += socket.inet_aton(host)
        except socket.error:
            resolve = True
            req += b"\x00\x00\x00\x01"
        # User ID
        req += b"pystocking\x00"
        # Host
        if resolve:
            if isinstance(host, str):
                host = host.encode("UTF-8")
            req += host + b"\x00"

        self.sendall(req)

        res = b""
        while len(res) < 8:
            res += self.recv(8 - len(res))
        if res[0] != 0x00 or res[1] != 0x5a:
            raise socket.error("Socks connection failed.")
