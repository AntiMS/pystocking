PyStocking is a simple SOCKS4a library for Python version 3.

PyStocking may be extended to support SOCKS4 and/or SOCKS5 at some point.

Installing
==========
Install using setup.py::

    > ./setup.py build
    > ./setup.py install

Using
=====
Using PyStocking is as simple as::

    > import pystocking
    > sock = pystocking.Socks4aSocket("socks_server.example.com", 9050)
    > sock.connect("remote_host.example.com", 8080)

Once the socket is connected to the remote host, you can send and receive data
via the .send() and .recv() methods as you would with any TCP socket.

Legal Stuff
===========
Copyright 2014 Richard Anderson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You shoudl have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
